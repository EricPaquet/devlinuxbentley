#!/bin/bash

# aliases
. $STDDIR/env/.bash_aliases

# Add bin to path (if not already there)
[[ ":$PATH:" != *":$STDDIR/bin:"* ]] && PATH="${PATH}:$STDDIR/bin"

# Process private std
if [ -f $STDDIR_PRIV/env/setup.sh ]; then
    . $STDDIR_PRIV/env/setup.sh
fi
