

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias cd-="cd -" # does not seem to work
alias cd..='cd ..'

#
# Utility aliases
#
alias ba='. ~/.bashrc'
alias linux_ver='lsb_release -a'
alias linux_kernel_ver='uname -a'
alias linux_kernel_ver2='uname -mrs'
alias edit_alias='vi $STDDIR/env/.bash_aliases'
alias cdstd='cd $STDDIR'

#
# Linux
#
alias lx_list_groups='cat /etc/group'
alias pes='printenv | sort'
alias diskusage='du -hs * | sort -h' # Sorted disk usage
alias es='env | sort'
alias gpu='lspci | grep -i "vga\|3d\|2d"'
alias lx_find_file='find' # linux - find [dir] [-name | ...] [file_name]
alias lx_kill_process='kill -signal' # linux - kill -signal [pid]. Kill a given process.
alias lx_list_process='ps aux' # linux - list all processes.
alias ls_sym='ll | grep "\->"' # list all symlinks
alias fa=findall
alias cat_libs_path='cat /etc/ld.so.conf.d/x86_64-linux-gnu.conf' # file containing paths where to find libs
alias ali='alias | grep -i'
alias sudo_admin='sudo -s' # or sudo su root
alias tar_compress='tar -zcvf [archive-name.tar.gz] [directory-name]'
alias tar_extract='tar xvzf [compressedFile.tar.gz]'
alias show_display='sudo lshw -C display'
alias tar_list='tar -ztvf [compressedFile.tar.gz]' # List contents of tar.gz

#
# apt
#
alias aptli='apt list --installed'
alias aptcs='apt-cache search'

#
# git
#
alias gits='git status'

#
# Docker
#
alias dpa='docker ps -a'
alias drm='docker rm $(docker ps -a -q)'
alias drmi='docker rmi $(docker images -q)'
alias di='docker images'
alias dkinfo='docker inspect'
alias dosa='docker stop $(docker ps -q)' # Stop all containers
# Examples
alias do_ex_run_bash='docker run --rm -it gcc /bin/bash'
alias do_ex_start_container='docker container start -i container_name'

# Specifically for Bentley Linux machine
alias cdeng1='cd /home/user/dev/DevCC/CCEngine1'

