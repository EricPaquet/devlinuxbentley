# PowerShell

# Alias
- `Get-ChildItem` : `dir`
- `Get-ChildItem` : `gci`

# Commands
- `dir env:` : show environment variables
- `dir -Force` : show hidden files
- `gci function:\` : list custom functions
- `Get-Variable` : Gets the variables in the current console. Alias: `gv`
- `Write-Output "State: $state"` : write to console (quotes around the variables produce output on the same line)
