# Notes on git

## Pull from a local repo
- ~/repo1 $ git remote add repo2 ~/repo2
- ~/repo1 $ git fetch repo2
- ~/repo1 $ git merge repo2/foo

## Push to other repo
git remote add ac_azure https://bentleycs.visualstudio.com/Reality%20Modeling/_git/acute3d
git pull ac_azure feature/Linux
git push ac_azure feature/Linux

### Example
- user@NAOU10922:~/dev/DevCC/cc3Linux/acute3d$ `git remote add cc1copy2 ~/devDivers/CC1_copy2/acute3d`
- user@NAOU10922:~/dev/DevCC/cc3Linux/acute3d$ `git fetch cc1copy2`
- user@NAOU10922:~/dev/DevCC/cc3Linux/acute3d$ `git merge cc1copy2/master`

## Git credentials
The point of this helper is to reduce the number of times you must type your username or password. For example:

$ git config credential.helper store
$ git push http://example.com/repo.git
Username: <type your username>
Password: <type your password>

[several days later]
$ git push http://example.com/repo.git
[your credentials are used automatically]

(Ref. https://git-scm.com/docs/git-credential-store)

## How to squash
- `git checkout master`
- `git pull`
- `git checkout -b my_new_branch`
- `git rebase a_branch_with_work` : can't squash directly a_branch_with_work if it was already pushed
- `git rebase -i HEAD~n` : n is the number of commits to squash. E.g. if 5 commits are to be squashed into 1, n = 4. In the file to edit, replace `pick` by `squash` for all except oldest commit. Normally, oldest commit is the one at the top.

(See also https://www.devroom.io/2011/07/05/git-squash-your-latests-commits-into-one/)

## Commands
- `git branch` : Indicate the branch on which you work (with a *)
- `git branch -a` : List all branches (local and remote)
- `git branch -d the_local_branch` : delete a local branch
- `git checkout .` : Revert all local uncommitted changes (should be executed in repo root)
- `git checkout [some_dir | file.txt]` : Revert uncommitted changes to a file or directory
- `git checkout <myBranch>` : switch to branch `myBranch`
- `git checkout -b [name_of_your_new_branch]` : create a new branch
- `git clean -f[d]` : Remove untracked files [and directories]
- `git config -l` : List all config values
- `git config --get core.autocrlf` : get the value for a particular setting
- `git config --list --show-origin` : Show git config in every config files (with config file path !)
- `git diff [commitId]~ [commitId]` : Diff between commit and predecessor
- `git diff --no-index [file1 file2]` : Diff between two untracked files. E.g. `git diff --no-index Downloads/file1.txt dev/file1.txt`
- `git diff --stat --cached origin/master` : Show files to be pushed
- `git diff origin/feature/Linux` : Code diff of the files to be pushed
- `git diff HEAD~` : Diff with the commit before the tip. E.g. use this to know what is in the last commit.
- `git diff branch1..branch2` : Diff between two branches.
- `git log --name-only` : show changed files in log
- `git log -p -- <file>` : show commits of the particular file with diffs for each change
- `git rebase master` : Rebase master on the current branch
- `git reset` : Unstage all files that were staged with `git add`
- `git reset HEAD~` : Undo last commit
- `git reset --soft HEAD~1` : Undo last commit (not sure what --soft does)
- `git reset --hard HEAD` : Abort merge
- `git remote show origin` : Determine URL where git push, pull, ...
- `git stash` to set the repo to a clean state (and save current work)
- `git stash apply` : to recover the unstaged changes
- `git stash drop` : Remove a single stash entry from the list of stash entries (the one at stash@{0})
- `git stash list` List the stash entries that you currently have.
- `git stash pop` : Remove a single stashed state from the stash list and apply it on top of the current working tree state, i.e., do the inverse operation of `git stash push`.
- `git stash push -m "tests for OIDC"` Do a stash with a message
- `git stash show -p [ stash@{1} ]` Show the changes recorded in the stash entry as a diff...
- `git status --ignored` : Show ignored files
- `git rev-parse --show-toplevel` : Show the root of the git repository

