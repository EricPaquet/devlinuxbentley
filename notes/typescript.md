TypeScript notes
================

# General Notes

## TypeScript Getting Started - Pluralsight Course

- Pluralsight: https://app.pluralsight.com/library/courses/getting-started-typescript/table-of-contents
- github repo: https://github.com/bricewilson/TypeScript-Getting-Started

# Install

## Install TypeScript
npm install -g typescript

## Install webpack
npm install --save-dev webpack

(--save-dev saves in devDependencies : webpack is not needed for those using the package. However, it is exposed for those who would *develop* with the package.)

# Configuration

## tsconfig.json - Useful options
    'watch": true 
    'strictNullChecks' : when set to true, null and undefined are not valid for any type. Default is false.
    Useful to catch usage of undefined variables, among others.
    'noImplicitAny': catches implicit anys in functions, e.g. function f1(val1, val2) {}
    'outFile': "../js/app.js" // Indicate which file contains the compiled js.
    'moduleResolution' // value: Classic | Node. Indicates how to handle module resolution.
    'traceResolution' : set to true to get more information from the compiler (e.g. where non-relative imports are searched)
    'baseUrl' : set a base directory where non-relative imports can be searched

## Configuration Inheritance
"extends": "../tsconfig.base"  // to inherit from tsconfig.base

## Webpack
Config file: webpack.config.js

## Compiler (tsc)

`tsc`: Start the compiler  
`tsc --init`: Initialize a new tsconfig.json  

# Build-in Types

## Basic Types
Boolean, Number, String, ...

'Any' type: you can assign any value to the variable.

## Union Types
let someValue: number | string; // Can take number or string value

## Type Assertions
    let value: any = 5;
    let fixedString: string = (<number>value).toFixed(4);

# Interface
    interface Employee {
        name: string;
        title: string;
        age?: number;   // Optional member; not required for the implementer
    }
    interface Manager extends Employee {
        department: string;
        scheduleMeeting: (topic: string) => void;
    }

    // Instanciation  
    let emp1: Employee = {name: 'Eric', title: 'Mr', age: 30};

    let emp1: Employee = 'ff'

# Class

## Example

    class Developer {
        department: string;
        private _title: string;
        get title(): string {
            return this._title;
        }
    }

    //Instanciate a class
    const firstPlayer: Player = new Player();
    firstPlayer.name = 'Eric';
    ...

Can use `extends` keyword to extend a class (inherit).  
Can use `implements` keyword to implement an interface.  

## Static member
    class ... {
        static jobDesc: string = 'Build things';
    }

## Constructor
    class Developer {
        constructor(prop: string) {
            super(); // Call parent constructor
            this.myProp = prop;
            console.log('...');
        }
    }

### Parameter property
    class Game {
        constructor(public player: Player, public problemCount: number) {
            // player, problemCount properties are automatically initialized
        }
    }


## Reference a file (i.e. include)
Place this (3 /s) at the begining of the file:

    /// <reference path="person.ts" />

**Hint**: just type `ref` for a shortcut.

# Function

## Default Initialized Parameters
function sendGreeting(greeting: string = 'Good morning!'): void {}

## Arrow Function (aka lambdas)
e.g. 
let squareit = x => x * x;
let result = squareit(4);

let adder = (a, b) => a + b;

let greeting = () => console.log('hi');

### Useful for
Functions called once (or not often)
Pass anonymous function to another function

## Assign a type 'function' to a variable
let logger: (value: string) => void // The type of logger is a function
...
function logError(err: string): void {console.error(err);}
logger = logError // Assign a function to logger

# Module

## Why use ?
- Encapsulation
- Reusability
- Create higher-level abstractions

## Exporting a declaration

    // person.ts
    export interface Person {}
    export function hireDeveloper(): void {}
    export default classe Employee {} // default item that will be exported from this module (if importer does not specify any)

### Importing from a Module

    import {Person, hireDeveloper} from './person';

    // non-relative import
    import * as $ from 'jquery';
    import * as lodash from 'loadash';

# Type Declaration Files

Usage: to declare types for variables, functions, etc.

Extension: .d.ts

## Obtain Type Declaration Files
- Look in node_modules
- Github DefinitelyTyped

## Installation
- Installed with npm

# Good practice

## Variable Declaration
Generally use "let" instead of "var". When possible, use "const".

## Declare the type
let x: string = 'My string.';

## Use Type Annotations with Functions
function funFunc(score: number, message?: string): string {}

## Classes
- Declare each class / interface in its own file
