# Notes on Docker

## Linux post-installation
### Eliminate 'warning config.json permission denied...' for docker
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R  
sudo chmod g+rwx /home/"$USER"/.docker -R

## build Dockerfile for gcc
docker build -t myimage:0.1 .
or (other example):  
cd to ~/dev/std/dockergcc1  
docker build -t gcc1 .
### Try the container
$ docker run --rm gcc1  
This should write 'Hello, world!' (or whatever message that was built in gcc1)

## Create a volume
docker volume create --name=vol1 

### bind mount (using -v option)
Example: `docker run --name my_cont -p 8080:80 -v ~/dev/std/dockergcc1/httpd/public-html:/usr/share/nginx/html -d nginx`. This binds a mount. When container my_cont is ran, it mounts the contents of .../public-html in the container (in /usr/.../html). When the container is restarted, it reuses the actual contents of public-html. Nothing is persisted in the container.

## Docker with node.js
### Start node and print to the console
docker run --rm -it --name=hj -v ~/dev/node/hello:/usr/src/app -w /usr/src/app node node hello.js  
### Run mini node server in dev/node/server
docker run -p 44476:8080 -d --name c4 --rm eric.paquet/node-server
#### Try the container in the browser
localhost:44476
#### Try the container with curl
curl -i localhost:44476
#### Reference
https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

## nginx
### Start nginx with a html file (index.html is in .../public-html)
docker run -p 8080:80 -v ~/dev/std/dockergcc1/httpd/public-html:/usr/share/nginx/html:ro -d --rm nginx  
#### Try the container in the browser
localhost:8080

## Troubleshoot
### Problems with `RUN apt-get update`
For example, it indicates that the server can't be reached.

I fixed this with [the solution here](https://stackoverflow.com/questions/24991136/docker-build-could-not-resolve-archive-ubuntu-com-apt-get-fails-to-install-a)

```
On the host (I'm using Ubuntu 16.04), find out the primary and secondary DNS server addresses:

$ nmcli dev show | grep 'IP4.DNS'
IP4.DNS[1]:              10.0.0.2
IP4.DNS[2]:              10.0.0.3
...
```

## Commands
- `docker run --name ep -p 8080:80 -v ~/dev/std/dockergcc1/httpd/public-html:/usr/share/nginx/html:ro -d nginx` : Name a container with --name

- `docker run --name ep -p 8080:80 -v ~/dev/std/dockergcc1/httpd/public-html:/usr/share/nginx/html -d nginx`
- `docker run --rm --name my_name -it my_image /bin/bash` : Run a container and start bash.
- `docker start my_container` : start a *container* (not image). It uses the parameters given when first ran.
- `docker exec -it my_container /bin/bash` : start a bash for a *running* container
- `docker images | grep "stuff_" | awk '{print $1 ":" $2}' | xargs docker rmi` : Delete images with wildcard

