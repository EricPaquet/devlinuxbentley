# BitBucket

# Use password app in command line

**TL;DR**
Enter this command: 
`git remote set-url origin https://EricPaquet:<PASSWORD APP>@bitbucket.org/EricPaquet/devlinuxbentley.git`

Example: `git remote set-url origin https://EricPaquet:xxxxxxxxx@bitbucket.org/EricPaquet/devlinuxbentley.git`

Then `git push` should work...

**Details**

From: <https://community.atlassian.com/t5/Bitbucket-articles/Announcement-Bitbucket-Cloud-account-password-usage-for-Git-over/ba-p/1948231/page/10>

@Flávio Neto Thank you for sharing your solution. I spent a lot of time searching for how to make this "New App Password" work and yours helped. This solution must be highlighted somewhere for people to save their time in future.

I will post it too, if someone sees my comment first. Go to the git bash terminal and paste the following command:

git remote set-url origin https://<USERNAME>:<PASSWORD>@bitbucket.org/path/to/repo.git
In the PASSWORD field a new App Password created in your bitbucket account should be put. 

