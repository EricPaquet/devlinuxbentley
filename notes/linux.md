# Useful linux hints

## apt
### search package
apt-cache search keyword

### remove boost
- Purge: `sudo apt-get --purge remove libboost-all-dev`
- Get the package containing boost: `dpkg -S /usr/include/boost/version.hpp`
- Remove this package (let's say libboost1.65-dev): `sudo apt-get autoremove libboost1.65-dev`

### list installed packages
apt list --installed

## Files
### Show hidden files
Type 'Ctrl-h' in File Explorer

### Compare files
Use `meld`

## Terminal
### Run custom command when starting the terminal
- In Terminal > Preferences menu, create a Profile
- In "Command" tab of new terminal, set "Run a custom command instead of my shell"
- Custom command example: sh -c ". /home/user/dev/std_priv/scripts/CC/cc_setup.sh; exec bash"
- When command exits: Exit the terminal
- To run with this profile, open a terminal and in File > New Terminal, select the desired profile 

### Open Nautilus as admin
- In a terminal, enter `sudo nautilus`

## Access windows share
### Ubuntu 18
- In File Explorer, click "Other Locations". 
- In field "Connect to Server", enter smb://bentley.com/shares/Temp
- user: eric.paquet 
- workgroup: BENTLEY
- password: regular Windows password

## OS details
### Is there any difference between setting an environment variable by EXPORT and by setenv?
export and not EXPORT is used by sh and ksh shells. setenv is used by csh.
The syntax also differs for both.
Code:
`export key=value`
`setenv key value`

## Change password
In a Terminal, type `passwd`

## Mount Windows shared folder
- 10.224.1.96 is Windows machine
- sudo mount -t cifs //10.224.1.96/devcc_share /mnt/win -o user=eric.paquet

### Better use this (set permissions)
- sudo mount -t cifs //10.224.1.96/devcc_share/ccJobQueue /mnt/winjob  -o user=eric.paquet,uid=user,gid=user,forceuid,forcegid

- other example for multi-engine: 
`sudo mount -t cifs //10.224.1.64/Users/Nicolas.Beland/Desktop/test ./share_windows/  -o user=nicolas.beland,uid=maxime,gid=maxime,forceuid,forcegid`

## Access Ubuntu shared folder from Windows

### Set up samba
sudo apt-get install samba

#### Set password (latest: p...)
smbpasswd -a linux_machine_user # e.g. smbpasswd -a user

edit /etc/samba/smb.conf
sudo service smbd restart

(See https://www.howtogeek.com/176471/)

### On Windows
- In command prompt (Windows): `net use X: \\10.224.1.44\sambashare /USER:user /P:yes`  
**Note** When asked for password, it is the password given to smbpasswd above (e.g. p...)

`sambashare` is the shared folder on Linux (defined in smb.conf)

- See https://www.codeproject.com/Questions/265459/HOW-TO-ACCESS-A-SHARED-FOLDER-VIA-COMMAND-LINE

## Display system
`echo $XDG_SESSION_TYPE` to know which display system is used (e.g. x11, wayland, ...)

## ssh
### E.g. copy file from local to remote machine (on AWS)
`scp -i "zTest_cle1.pem" fic1.txt ubuntu@ec2-13-250-34-125.ap-southeast-1.compute.amazonaws.com:/home/ubuntu`

## Libraries

### Get info about a library
- `file [library]` - Example: `file /usr/lib/libceres.so.1.13.0`
- `ldd [library]` - Show dependencies (library must be a .so) Example: ldd libgdal.so
- `nm -gC [library.a]` - Show exported symbols
- `objdump -TC [library.so]` - Show exported symbols (.so file in elf format)

## Video card
`sudo lshw -C display` : find information about GPU, graphics card
`nvidia-smi` : show driver information if driver can be found

## Processes
### Start multiple instances in background
Example:  
`for i in $(seq 1 2000); do (./UtilsTest --gtest_filter=*Eric_05* >> toto.txt &); done`

## Commands
- `apt list --installed` : list installed packages
- `apt-cache search keyword` : search for a package
- `apt-cache showpkg keyword` : show a package version. Ex. `apt-cache showpkg libboost-all-dev`
- `apt-get install package` : install a package (use with sudo generally)
- `apt-get update` : it updates the package lists for upgrades for packages that need upgrading
- `cat /etc/group` : list groups
- `df [-h]`: return information about used and available disk space for all partitions accessible
- `dpkg -L [packagename]` : Indicate where files of a package are installed
- `du -sh [dirName]` : total size of a single directory
- `env | sort` : list exported environment variables
- `find [dir] [-name | -iname | ...] [file_name]` : find files (wildcards are required (e.g. *file*))
- `find / -iname file_name 2> /dev/null` : find all "file_name" files; `2> /dev/null` ignores errors
- `ip a`:  reveal the IP address currently used by the system.
- `ip r`:  obtain the IP address of your gateway.
- `kill -signal [pid]` : Kill a given process
- `locate [fileName]`: List all files or directories within the entire file-system. 
- `ls -Ralp | grep -v /` : List files only; recursive
- `ls -lt` : list by date
- `lsb_release -a` : linux version
- `lspci | grep -i 'vga\|3d\|2d'` : Indicate the machine's GPU ?
- `printenv | sort` : list environment variables
- `ps aux` : list all processes
- `rm -r *` : remove all files recursively (DANGEROUS !!!)
- `tar xvzf [compressedFile.tar.gz]` : extract a tar.gz file 
- `tar -zcvf [archive-name.tar.gz] [directory-name]` : compress a directory to a tar.gz file
- `trash-empty` : empty trash
- `uname -a` : linux kernel version
- `uname -mrs` : linux kernel version
- `whoami`: return a username of the user who executes it.
- `![command]` : execute last command that starts with the letters “command”.

