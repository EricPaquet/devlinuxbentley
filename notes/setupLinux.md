# Setup Linux on a new machine

## Time is wrong ?
If the time zone is not ok:  
$ sudo apt-get install ntp

## Install VS Code
Follow instructions here: https://code.visualstudio.com/docs/setup/linux

Install .deb package and then:
    sudo dpkg -i <file>.deb
    sudo apt-get install -f # Install dependencies
    sudo apt-get update
    sudo apt-get install code # or code-insiders

## Install Docker
Follow instructions here:
https://docs.docker.com/install/linux/docker-ce/ubuntu/
(Install using the repository)

### Run docker without always using sudo
See https://docs.docker.com/install/linux/linux-postinstall/ (Manage Docker as a non-root user)

## Install putty
sudo apt-get install putty

## To generate key-pairs (TO BE CONFIRMED)
generate private key with ssh-keygeon
cd to directory where keys were generated (most likely, ~/.ssh/)
Use puttygen command line to generate id_rsa.ppk: puttygen id_rsa -o id_rsa.ppk

