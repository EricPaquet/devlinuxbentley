
# npm notes

# Inspect an NPM package’s contents
---------------------------------

e.g. `npm view @itwin/reality-data-client`  
Then you get information for the tarball, e.g.:  
dist
.tarball: https://registry.npmjs.org/@itwin/reality-data-client/-/reality-data-client-0.7.1.tgz

Download this file, and extract it, using 7zip

# Build a npm package locally
---------------------------------

E.g. locally building reality-data-client for use in realitydata-sample-app:

- Build project to test:
`PS D:\Dev\itwin\reality-data-client> npm run build`
- Pack it:
`PS D:\Dev\itwin\reality-data-client> npm pack`
- Copy the resulting tgz file to the target project (i.e. copy it to `D:\Dev\realitydata-sample-app_forDev\itwin-reality-data-client-0.7.2.tgz`)
- In `D:\Dev\realitydata-sample-app_forDev\package.json`, set the tgz file as the version, e.g.:

```
  "dependencies": {
    ...
    "@itwin/reality-data-client": "itwin-reality-data-client-0.7.2.tgz", 
    ...
```

- Install the package:
`PS D:\Dev\realitydata-sample-app_forDev> npm install`

- **Note** When re-installing the package (i.e. updating the tgz file), it may be necessary to uninstall the tgz and re-install for changes to take effect.

Ref: <https://stackoverflow.com/questions/55560791/build-and-use-npm-package-locally>


# npx
---------------------------------

`npx create-react-app my-app --template typescript` : create a new React app with TypeScript

# Commands
---------------------------------

- `npm list -g --depth 0` : Get list of globally installed packages; don't list dependencies (`--depth 0`).
- `npm search reality-data-client` : Search npm repo for a package.
- `npm ls hoek` : For example, to see which packages are using Hoek
