# Visual Studio Code

## Use regular expressions to search code
E.g. "macro.*\(uselib" will find:
MACRO(UseLibraryDependencies lib)
MACRO (UseLibraryDependencies lib)
ENDMACRO(UseLibraryDependencies lib)
...

## Code formatting
You can format an entire file with Format Document (Ctrl+Shift+I) or just the current selection with Format Selection (Ctrl+K Ctrl+F) in right-click context menu.
You can also configure auto-formatting with the following settings:

- editor.formatOnSave - to format when you save your file.
- editor.formatOnType - to format as you type (triggered on the ; character).

## Editor
- How do I jump to a closing bracket in Visual Studio Code? `Windows Ctrl+Shift+\`
