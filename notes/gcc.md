# Notes on gcc

## Build a simple file
g++ hello.cc -o hello.exe

## gcc standards
C++14 Support in GCC

GCC has full support for the previous revision of the C++ standard, which was published in 2014.

This mode is the default in GCC 6.1 and above; it can be explicitly selected with the -std=c++14 command-line flag, or -std=gnu++14 to enable GNU extensions as well.

##  ADL
In the C++ programming language, argument-dependent lookup (ADL), or argument-dependent name lookup,[1] applies to the lookup of an unqualified function name depending on the types of the arguments given to the function call. This behavior is also known as Koenig lookup, as it is often attributed to Andrew Koenig, though he is not its inventor.[2]

## Build in parallel
Command: 'make -j 8'

## Debug
- gdb [myExecutable]
- (gdb) run
- (gdb) backtrace

### Most useful commands
- `help`
    Will give you help on most gdb functions. If you wish for help on a specific command, type help command. 
- `b function-name`
    To set a breakpoint at a function. 
- `r args`
    To run the program. It will run until it reaches a breakpoint. 
- `s`
    To single-step through lines of code. 
- `c`
    To continue until the next breakpoint. 
- `p variable`
    To print a variable's value. 
- `q`
    To quit gdb. 



