# How to build the image
docker build -t myhttpd .

# Run the container
docker run -d --rm -p 80:80 --name ht myhttpd

## Go to http://localhost:80 in the browser to see the static web page